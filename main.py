import socket
import random

urls = ['https://www.aulavirtual.urjc.es/', 'https://mail.google.com/mail/', 'https://www.zara.com/es/',
        'https://gitlab.eif.urjc.es/cursosweb/2023-2024', 'https://es.shein.com']

myPort = 1234

def create_socket(port):
    mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    mySocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    mySocket.bind(('localhost', port))
    return mySocket

def get_random_url():
    return random.choice(urls)

def serve_client_request(client_socket):
    print("HTTP request received:")
    request_data = client_socket.recv(2048).decode('utf-8')
    print(request_data)
    url = get_random_url()
    print("Redireccionando a:", url)
    response = f"HTTP/1.1 307 Temporary Redirect\r\nLocation: {url}\r\n\r\n"
    client_socket.send(response.encode('utf-8'))
    client_socket.close()

def main():
    mySocket = create_socket(myPort)
    mySocket.listen(5)

    try:
        while True:
            print("En espera de conexiones entrantes...")
            client_socket, address = mySocket.accept()
            serve_client_request(client_socket)

    except KeyboardInterrupt:
        print("Cerrando el socket.")
        mySocket.close()

if __name__ == "__main__":
    main()
